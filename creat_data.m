
% n: Probability of number of individuals prob Whether the third column is incubation period, whether the fourth column is feverish, whether the fifth column is diagnosed,
% Whether the sixth column is isolated, the seventh column is in the incubation period, and the eighth column is the number of days after the diagnosis.
function data=creat_data(n,prob)
data=zeros(n,8);
data(:,1:4)=rand(n,4);
safe_index=find(data(:,3)>prob);
unsafe_index=find(data(:,3)<prob);
data(safe_index,3)=0;
data(unsafe_index,3)=1;
end