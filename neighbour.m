% alpha can be defined as the threshold of virus transmission ability
function infected_mat = neighbour (data, beta)
dis = get_dis (data (:, 1: 2));
infected_mat = dis <beta;
% Get Isolated Person
infected_mat (find (data (:, 6) == 1),:) = 0;% in isolation is not infectious
infected_mat (:, find (data (:, 6) == 1)) = 0;% in isolation is not infectious
% infected_mat (find (data (:, 3) == 0),:) = 0;% is not infected and is not contagious
% infected_mat (:, find (data (:, 3) == 0)) = 0;% is not sick and not contagious
end