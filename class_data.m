

% n: number of individuals prob disease probability Whether the third column is in the incubation period, the fourth column is feverish, the fifth column is confirmed, the sixth column is isolated, and the seventh column is in the incubation period
function data=creat_data(n,prob)
data=zeros(n,7);
data(:,1:4)=rand(n,4);
safe_index=find(data(:,3)>prob);
unsafe_index=find(data(:,3)<prob);
data(safe_index,3)=0;
data(unsafe_index,3)=1;
end
% Personnel Movement Speed
function data=shift_speed(data,alpha)
theta=2*rand(n,2)-ones(n,2);
data(:,1:2)=data(:,1:2)-alpha*theta;
data(find(data(:,1)>1),1)=1;
data(find(data(:,1)<0),1)=0;
data(find(data(:,2)>1),2)=1;
data(find(data(:,2)<0),2)=0;
end
% Get the distance between two points
function dis=get_dis(data)
data=data(:,1:2);
dis=pdist2(data);
end
% alpha can be defined as the threshold for neighbors
function infected_mat=neighbour(data,beta)
data=data(:,1:2);
dis=get_dis(data);
infected_mat=dis<beta;
end
% Set contact with infected person to 1
function data=notify_infected(data,infected_mat)
for i=1:size(data,1)
    %��Ⱦ��
    if data(i,3)==1
        data(find(infected_mat(i,:)==1),3)=1;
    end
end
end
% Isolate the infected person, it is not contagious, so the found infected person is removed from the index 1xN matrix
function data =isolats(data,index)
data(index,3)=0;
end
% Plots infected vs. uninfected
function creat_plot(data)
safe_index=find(data(:,3)==0);
unsafe_index=find(data(:,3)==1);
plot(data(safe_index,1),data(safe_index,2),'r*');
plot(data(unsafe_index,1),data(unsafe_index,2),'b*');
end


    