%Get index for personnel in the incubation period
function period_index=get_period_index(data)
period_index=find(data(:,3)==1);
end
