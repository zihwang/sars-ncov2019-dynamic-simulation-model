% Set the virus incubation period to be confirmed, the fifth column is confirmed
function data=diagnose(data,period_day)
period_day=period_day+round((2*rand(1,1)-1)*period_day*0.4); 
data(find(data(:,7)>period_day),5)=1;
end