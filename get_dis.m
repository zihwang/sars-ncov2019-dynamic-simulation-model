% Stores the number of people in the daily historical indicators
function dis=get_dis(data)
data=data(:,1:2);
dis=pdist2(data,data);
end