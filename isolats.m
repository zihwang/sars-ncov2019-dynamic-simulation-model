% Isolate the infected person, it is not contagious, so the found infected person is removed from the index 1xN matrix
function data =isolats(data)
data(find(data(:,6)==1),3)=0;
end