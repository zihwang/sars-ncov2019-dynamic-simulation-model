% Increase the number of days in which the infected person is in the incubation period every day, and the seventh column is in the incubation period.
function data=update_at_period_day(data)
period_index=get_period_index(data);
data(period_index,7)=data(period_index,7)+1;
end