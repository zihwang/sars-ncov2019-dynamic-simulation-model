n = 1000;% of total population individuals
prob = 0.01;% initial prevalence
K = 1;% positive correlation coefficient between virus transmission capacity and personnel movement speed
alpha = 0.02;% personnel movement speed
beta = K * alpha;% virus transmission ability
data = creat_data (n, prob);% Initialize population
period_day = 10;% virus latency period
gamma = 100;% assuming treatment days greater than gamma can be cured

% Is used to store the number of people under various historical indicators
history_period_num = [];%
history_feve_num = [];%
history_diagnose_num = [0];%
history_crease_diagnose_num_per_day = [];%

% Of SIR model indicators
S = []; I = []; R = [];

for i = 1: 2000
% Government intervention time
    if i>= 3
        data = isolats (data);% Isolate those who have been found to be infected, not infectious
        % alpha = 0.01;% personnel movement speed
        % alpha = 0.000;% personnel movement speed
        alpha = 0.005;% personnel movement speed
        beta = K * alpha;% virus transmission ability
    end
	if sum (data (:, 3)) == size (data, 1)
        break
    end
	
	 %Update the individual position after a movement
	data = shift_speed (data, alpha);
	infected_mat = neighbour (data, beta);
	data = notify_infected (data, infected_mat);
	subplot (2,2,4)
	creat_plot (data);
	
	%% Increments the number of days the infected person is in the incubation period every day, and the seventh column is in the incubation period.
	data = update_at_period_day (data);
	 %Set the virus incubation period to be confirmed, the fifth column is confirmed
	data = diagnose (data, period_day);
	data = updata_treat_day (data);
    % Patients treated longer than gamma as safe
	data = treated_succession (data, gamma);

%
    [period_num, fever_num, diagnose_num] = get_all_index_num (data);
    history_period_num = [history_period_num, period_num];
	history_feve_num = [history_feve_num, fever_num];
	history_diagnose_num = [history_diagnose_num, diagnose_num];
	crease = history_diagnose_num (size (history_diagnose_num, 2))-history_diagnose_num (size (history_diagnose_num, 2) -1);
	history_crease_diagnose_num_per_day = [history_crease_diagnose_num_per_day, crease];

% Susceptible
	S_temp = size (data, 1) -sum (data (:, 3));
	S = [S, S_temp];
	I_temp = sum (data (:, 3))-sum ((data (:, 6)));
	I = [I, I_temp];
	R_temp = sum (data (:, 6));
	R = [R, R_temp];

	plot_index (history_period_num, history_crease_diagnose_num_per_day, history_diagnose_num, S, I, R);

	pause (0.1)
end